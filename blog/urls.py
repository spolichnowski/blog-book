from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^articles/', include(
                        'articles.urls',
                        namespace='articles',
                        app_name='articles'
                        )),
    #url(r'^account/', include('account.urls')),
]
