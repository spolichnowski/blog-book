from django.core.mail import send_mail
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import get_object_or_404, render

from taggit.models import Tag
from .models import Post
from .forms import EmailPostForm


def post_list(request, tag_slug=None):
    object_list = Post.published.all()
    tag = None

    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        object_list = object_list.filter(tags__in=[tag])

    paginator = Paginator(object_list, 3)
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    return render(
        request,
        'templates/articles/list.html',
        {
            'page': page,
            'posts': posts,
            'tag': tag
        }
    )


def post_detail(request, year, month, day, post):
    post = get_object_or_404(
        Post,
        slug=post,
        status='published',
        publish__year=year,
        publish__month=month,
        publish__day=day
    )

    return render(
        request,
        'templates/articles/detail.html',
        {'post': post}
    )


def post_share(request, post_id):
    post = get_object_or_404(Post, id=post_id, status='published')
    sent = False

    if request.method == 'POST':
        form = EmailPostForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            post_url = request.build_absolute_uri(
                                            post.get_absolute_url()
            )
            subject = '{} ({}) recommends you reading "{}"'.format(
                cd['name'],
                cd['email'],
                post.title
            )
            message = 'Read "{}" at {}\n\n{}\'s comments: {}'.format(
                post.title,
                post_url,
                cd['name'],
                cd['comments']
            )
            send_mail(subject,
                      message,
                      'admin@myblog.com',
                      [cd['to']]
            )
            sent = True
    else:
        form = EmailPostForm()
    return render(
        request,
        'templates/articles/share.html',
        {
            'post': post,
            'form': form,
            'sent': sent
        })
