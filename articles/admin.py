from django.contrib import admin
from .models import Post


class PostAdmin(admin.ModelAdmin):
    list_display = (
                    'title',
                    'slug',
                    'author',
                    'publish',
                    'status'
                    )
    list_filter = (
                    'author',
                    'publish',
                    'status'
                    )
    ordering = ['status', 'publish']
    prepopulated_fields = {'slug': ('title', )}


admin.site.register(Post, PostAdmin)
